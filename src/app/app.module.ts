import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { TicketComponent } from './ticket/ticket.component';
import {TicketService} from './services/ticket.service'

@NgModule({
  declarations: [
    AppComponent,
    TicketComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [TicketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
