import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators'
import {Ticket} from '../model/ticket.model'
import { Observable, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
 
  public _apiUrl:"http://localhost:59540/";
  ticket:Ticket[];
  subscription:Subscription

  constructor(private _http:HttpClient) { }

  public getAllTickets():Observable<Ticket[]>{   
    const result = this._http.get<Ticket[]>("http://localhost:59540/api/Ticket");
    return result;
  }

  public getTicketById(){

  }

  public saveTicket(){

  }

  public deleteTicket(){

  }

}
