import { Component, OnInit } from '@angular/core';

import { Ticket } from '../model/ticket.model'
import { TicketService } from '../services/ticket.service'
import { log } from 'util';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

  ticket: Ticket = new Ticket;
  ticket2:Ticket = new Ticket;
  tickets: Ticket[]
  show: boolean
  dataTest: any
  
  testIcon:any;

  constructor(private ticketService: TicketService) {
  }

  ngOnInit() {
 
  }

  ShowTickets() {
    this.ticket.status = '1'
    this.ticket.priority = 2
    this.ticket.email = 'a@a.com'
    this.ticket.subject = 'Test Subject'
    this.ticket.description = 'This is description'

    this.ticket2.status = '1'
    this.ticket2.priority = 2
    this.ticket2.email = 'a@a.com'
    this.ticket2.subject = 'Test Subject'
    this.ticket2.description = 'This is description'

    this.tickets = [this.ticket,this.ticket2];
    this.show = true;
  }

  ShowAllTickets() {
    this.ticketService.getAllTickets().subscribe(res=>this.tickets=res,error=>console.log(error));
  }
}
