export class Ticket{
    status:string
    priority:number
    email:string
    subject:string
    description:string
}